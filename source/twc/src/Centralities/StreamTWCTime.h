#ifndef TGPR_WALKCENTRALITYSTREAMNEWVERSIONTIME_H
#define TGPR_WALKCENTRALITYSTREAMNEWVERSIONTIME_H

#include "../TemporalGraph/TemporalGraphs.h"
#include "../Helpers/ResultFile.h"

Result calculateWalkCentralityStreamNewVersionTime(TemporalGraphStream const &tgs, Params const &params);

#endif //TGPR_WALKCENTRALITYSTREAMNEWVERSIONTIME_H
