#ifndef TGPR_WALKCENTRALITYDLGMATRIXNEWVERSION_H
#define TGPR_WALKCENTRALITYDLGMATRIXNEWVERSION_H

#include "../TemporalGraph/TemporalGraphs.h"
#include "../Helpers/ResultFile.h"
#include "TemporalGraph/DirectedLineGraph.h"

Result calculateWalkCentralityDLGMatrixNewVersion(TemporalGraphStream &tgs, TemporalGraph &tg, Params const &params);

Result calculateWalkCentralityDLGMatrixApproximationNewVersion(TemporalGraphStream &tgs, TemporalGraph &tg, Params const &params);

Result calculateWalkCentralityDLGMatrixNewVersionNonSparse(TemporalGraphStream &tgs, TemporalGraph &tg, Params const &params);


#endif //TGPR_WALKCENTRALITYDLGMATRIXNEWVERSION_H
