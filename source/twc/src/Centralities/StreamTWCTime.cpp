#include "StreamTWCTime.h"
#include "../Helpers/SGLog.h"
#include <map>
#include <list>

using namespace std;

vector<map<Time, double>> getIncomingWeightedWalksTime(TemporalGraphStream const &tgs, double alpha) {
    vector<map<Time, double>> walks(tgs.num_nodes, map<Time, double>());
    for (auto &e : tgs.edges) {
        walks.at(e.v_id)[e.t+1] += 1; //alpha;
        for (auto &x : walks.at(e.u_id)) {
            if (x.first > e.t) {
                continue;
            }
            walks.at(e.v_id)[e.t + 1] += (alpha/(2.0+(double)e.t-(double)x.first) * x.second);
        }
    }
    return walks;
}

vector<map<Time, double>> getOutgoingWeightedWalksTime(TemporalGraphStream const &tgs, double beta) {
    vector<map<Time,double>> walks(tgs.num_nodes, map<Time,double>());
    for (unsigned long ep = tgs.edges.size()-1; ; --ep) {
        auto &e = tgs.edges[ep];
        walks.at(e.u_id)[e.t] += 1; //beta;
        for (auto &x : walks.at(e.v_id)) {
            if (x.first < e.t + 1) {
                continue;
            }
            walks.at(e.u_id)[e.t] += (beta/(2.0+abs((double)x.first)-(double)e.t) * x.second);
        }
        if (ep == 0) break;
    }
    return walks;
}

vector<double> computeCentralityTime(TemporalGraphStream const &tgs, vector<map<Time, double>> &ifct, vector<map<Time, double>> &ofct) {
    vector<double> centrality(tgs.num_nodes, 0);

#pragma omp parallel for default(none) shared(centrality, tgs, ifct, ofct)
    for (NodeId nid = 0; nid < tgs.num_nodes; ++nid) {

        auto rt = ifct[nid].size() < ofct[nid].size() ? ifct[nid] : ofct[nid];
        auto ct = ifct[nid].size() < ofct[nid].size() ? ofct[nid] : ifct[nid];

        auto init = ct.begin();
        auto outit = rt.begin();

        while (outit != rt.end()) {
            for (auto &i : ct) {
                if (i.first > outit->first) break;
                centrality[nid] += ct[i.first] * outit->second * (1.0/(1.0+(double)outit->first-(double)i.first));
            }
            ++outit;
        }
    }
    return centrality;
}

Result calculateWalkCentralityStreamNewVersionTime(TemporalGraphStream const &tgs, Params const &params) {

    TopkResult topkResult(params.k, tgs.num_nodes);

    SGLog::log() << "Stream (time):" << endl;

    auto start = std::chrono::high_resolution_clock::now();
    vector<double> centrality;

    auto outwalks = getOutgoingWeightedWalksTime(tgs, params.beta);
    auto inwalks = getIncomingWeightedWalksTime(tgs, params.alpha);

    centrality = computeCentralityTime(tgs, inwalks, outwalks);

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "Elapsed time: " << elapsed.count() << " s\n";

    std::vector<std::pair<NodeId, double>> all_results;
    for (NodeId i = 0; i < tgs.num_nodes; ++i) {
        all_results.emplace_back(i, centrality.at(i));
        topkResult.insert(centrality.at(i), i);
    }

    topkResult.print();

    Result result(elapsed.count(), {0, 1}, topkResult);
    result.all_results = all_results;
    return result;
}