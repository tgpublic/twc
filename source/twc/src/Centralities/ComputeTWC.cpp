#include "ComputeTWC.h"
#include <map>

using namespace std;


vector<double>
computeCentrality(TemporalGraphStream const &tgs, vector<map<Time, double>> &ifct, vector<map<Time, double>> &ofct) {
    vector<double> centrality(tgs.num_nodes, 0);

#pragma omp parallel for default(none) shared(centrality, tgs, ifct, ofct)
    for (NodeId nid = 0; nid < tgs.num_nodes; ++nid) {

        auto init = ifct[nid].begin();
        auto outit = ofct[nid].begin();

        double insum = 0;

        while (outit != ofct[nid].end()) {
            if (init != ifct[nid].end() && outit != ofct[nid].end() && init->first < outit->first) {
                insum += init->second;
                ++init;
            }

            if (init != ifct[nid].end() && outit != ofct[nid].end() && outit->first < init->first) {
                if (init->first <= outit->first)
                    insum += init->second;
                centrality[nid] += insum * outit->second;
                ++outit;
            }
            if (init != ifct[nid].end() && outit != ofct[nid].end() && init->first == outit->first) {
                if (init->first <= outit->first)
                    insum += init->second;
                centrality[nid] += insum * outit->second;
                ++init;
                ++outit;
            }

            if (init == ifct[nid].end() && outit != ofct[nid].end()) {
                while (outit != ofct[nid].end()) {
                    centrality[nid] += insum * outit->second;
                    ++outit;
                }
                break;
            }
        }
    }
    return centrality;
}

