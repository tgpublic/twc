#include "StreamTWC.h"
#include "../Helpers/SGLog.h"
#include "ComputeTWC.h"
#include <map>
#include <list>

using namespace std;

vector<map<Time, double>> getIncomingWeightedWalks(TemporalGraphStream const &tgs, double alpha) {
    vector<map<Time, double>> walks(tgs.num_nodes, map<Time, double>());
    for (auto &e : tgs.edges) {
        walks.at(e.v_id)[e.t+1] += 1; //alpha;
        for (auto &x : walks.at(e.u_id)) {
            if (x.first > e.t) {
                continue;
            }
            walks.at(e.v_id)[e.t + 1] += (alpha * x.second);
        }
    }
    return walks;
}

vector<map<Time, double>> getOutgoingWeightedWalks(TemporalGraphStream const &tgs, double beta) {
    vector<map<Time,double>> walks(tgs.num_nodes, map<Time,double>());
    for (unsigned long ep = tgs.edges.size()-1; ; --ep) {
        auto &e = tgs.edges[ep];
        walks.at(e.u_id)[e.t] += 1; //beta;
        for (auto &x : walks.at(e.v_id)) {
            if (x.first < e.t + 1) {
                continue;
            }
            walks.at(e.u_id)[e.t] += (beta * x.second);
        }
        if (ep == 0) break;
    }
    return walks;
}

Result calculateWalkCentralityStreamNewVersion(TemporalGraphStream const &tgs, Params const &params) {

    TopkResult topkResult(params.k, tgs.num_nodes);

    SGLog::log() << "Stream:" << endl;

    auto start = std::chrono::high_resolution_clock::now();
    vector<double> centrality;

    auto outwalks = getOutgoingWeightedWalks(tgs, params.beta);
    auto inwalks = getIncomingWeightedWalks(tgs, params.alpha);

    centrality = computeCentrality(tgs, inwalks, outwalks);

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "Elapsed time: " << elapsed.count() << " s\n";

    std::vector<std::pair<NodeId, double>> all_results;
    for (NodeId i = 0; i < tgs.num_nodes; ++i) {
        all_results.emplace_back(i, centrality.at(i));
        topkResult.insert(centrality.at(i), i);
    }

    topkResult.print();

    Result result(elapsed.count(), {0, 1}, topkResult);
    result.all_results = all_results;
    return result;
}