#include "DLGMatrixTWC.h"
#include "../Helpers/SGLog.h"
#include "TemporalGraph/DirectedLineGraph.h"
#include "ComputeTWC.h"
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;


pair<vector<map<Time, double>>, vector<map<Time, double>>>
computeVecMatricesInf1(DirectedLineGraph const &dlg, const Params &params) {

    vector<map<Time, double>> inwalks(dlg.num_tg_nodes);
    vector<map<Time, double>> outwalks(dlg.num_tg_nodes);

    typedef Eigen::Triplet<double> T;
    std::vector<T> coefficients;

    for (const auto &n: dlg.nodes) {
        for (const auto &edge: n.outedges) {
            int a = edge->u;
            int b = edge->v;
            T coef{a, b, 1};
            coefficients.push_back(coef);
        }
    }
    SparseMatrix<double> mat_ones(dlg.nodes.size(), dlg.nodes.size());
    mat_ones.setFromTriplets(coefficients.begin(), coefficients.end());
    SparseMatrix<double> I(dlg.nodes.size(), dlg.nodes.size());
    I.setIdentity();
    {
        auto mat = mat_ones * params.beta;
        SparseMatrix<double> R = (I - mat);
        SparseLU<SparseMatrix<double> > solver;
        solver.compute(R);
        SparseMatrix<double> R_inv = solver.solve(I);

        VectorXd w(dlg.nodes.size());
        w.setOnes();
        w = R_inv * w;

        for (unsigned int j = 0; j < w.size(); ++j) {
            auto num_walks = w[j];
            auto &dlg_u = dlg.nodes[j];
            outwalks[dlg_u.e.u_id][dlg_u.e.t] += num_walks;
        }
    }
    {
        SparseMatrix<double> mat_trans = (mat_ones * params.alpha).transpose();
        SparseMatrix<double> R2 = (I - mat_trans);
        SparseLU<SparseMatrix<double> > solver2;
        solver2.compute(R2);
        auto R2_inv = solver2.solve(I);

        VectorXd w(dlg.nodes.size());
        w.setOnes();
        w = R2_inv * w;
        for (unsigned int j = 0; j < w.size(); ++j) {
            auto num_walks = w[j];
            auto &dlg_v = dlg.nodes[j];
            inwalks[dlg_v.e.v_id][dlg_v.e.t + dlg_v.e.traversal_time] += num_walks;
        }
    }
    return {inwalks, outwalks};
}


Result calculateWalkCentralityDLGMatrixNewVersion(TemporalGraphStream &tgs, TemporalGraph &tg, Params const &params) {
    TopkResult topkResult(params.k, tgs.num_nodes);

    SGLog::log() << "Walk centrality (DLG Matrix):" << endl;

    Timer timer;

    SGLog::log() << "computing DLG" << endl;

    timer.start();
    vector<double> centrality(tgs.num_nodes, 0);
    auto dlg = temporalGraphToDirectedLineGraph(tgs, tg);
    timer.stopAndPrintTime();

    SGLog::log() << "#nodes: " << dlg.nodes.size() << endl;
    SGLog::log() << "#edges: " << dlg.num_edges << endl;

    auto walks = computeVecMatricesInf1(dlg, params);
    auto outwalks = walks.second;
    auto inwalks = walks.first;

    centrality = computeCentrality(tgs, inwalks, outwalks);

    auto final_time = timer.stopAndPrintTime();

    std::vector<std::pair<NodeId, double>> all_results;
    for (int i = 0; i < tgs.num_nodes; ++i) {
        all_results.emplace_back(i, centrality.at(i));
        topkResult.insert(centrality.at(i), i);
    }

    topkResult.print();

    Result result(final_time, {0, 1}, topkResult);
    result.all_results = all_results;
    return result;
}


pair<vector<map<Time, double>>, vector<map<Time, double>>>
computeVecMatricesApprox(DirectedLineGraph const &dlg, const Params &params, Time delta) {

    vector<map<Time, double>> inwalks(dlg.num_tg_nodes);
    vector<map<Time, double>> outwalks(dlg.num_tg_nodes);

    typedef Eigen::Triplet<double> T;
    std::vector<T> coefficients;

    for (const auto &n: dlg.nodes) {
        for (const auto &edge: n.outedges) {
            int a = edge->u;
            int b = edge->v;
            T coef{a, b, 1};
            coefficients.push_back(coef);
        }
    }
    SparseMatrix<double> mat_ones(dlg.nodes.size(), dlg.nodes.size());
    mat_ones.setFromTriplets(coefficients.begin(), coefficients.end());

    {
        auto mat = mat_ones * params.beta;
        VectorXd w(dlg.nodes.size());
        w.setOnes();
        auto m = w;
        while (w.lpNorm<1>() > params.epsilon) {
            w = mat * w;
            m += w;
        }
        for (unsigned int j = 0; j < m.size(); ++j) {
            auto num_walks = m[j];
            auto &dlg_u = dlg.nodes[j];
            outwalks[dlg_u.e.u_id][dlg_u.e.t] += num_walks;
        }
    }

    {
        SparseMatrix<double> mat = (mat_ones * params.alpha).transpose();
        VectorXd w(dlg.nodes.size());
        w.setOnes();
        auto m = w;
        while (w.lpNorm<1>() > params.epsilon) {
            w = mat * w;
            m += w;
        }
        for (unsigned int j = 0; j < m.size(); ++j) {
            auto num_walks = m[j];
            auto &dlg_v = dlg.nodes[j];
            inwalks[dlg_v.e.v_id][dlg_v.e.t + delta] += num_walks;
        }
    }
    return {inwalks, outwalks};
}


Result calculateWalkCentralityDLGMatrixApproximationNewVersion(TemporalGraphStream &tgs, TemporalGraph &tg,
                                                               Params const &params) {
    TopkResult topkResult(params.k, tgs.num_nodes);

    SGLog::log() << "Walk centrality (DLG Matrix - Approximation):" << endl;

    Timer timer;

    SGLog::log() << "computing DLG" << endl;

    timer.start();
    vector<double> centrality(tgs.num_nodes, 0);
    auto dlg = temporalGraphToDirectedLineGraph(tgs, tg);
    timer.stopAndPrintTime();

    SGLog::log() << "#nodes: " << dlg.nodes.size() << endl;
    SGLog::log() << "#edges: " << dlg.num_edges << endl;

    auto walks = computeVecMatricesApprox(dlg, params, params.delta);
    auto outwalks = walks.second;
    auto inwalks = walks.first;

    centrality = computeCentrality(tgs, inwalks, outwalks);

    auto final_time = timer.stopAndPrintTime();

    std::vector<std::pair<NodeId, double>> all_results;
    for (int i = 0; i < tgs.num_nodes; ++i) {
        all_results.emplace_back(i, centrality.at(i));
        topkResult.insert(centrality.at(i), i);
    }

    topkResult.print();

    Result result(final_time, {0, 1}, topkResult);
    result.all_results = all_results;
    return result;
}


pair<vector<map<Time, double>>, vector<map<Time, double>>>
computeVecMatricesInf1NonSparse(DirectedLineGraph const &dlg, const Params &params) {

    vector<map<Time, double>> inwalks(dlg.num_tg_nodes);
    vector<map<Time, double>> outwalks(dlg.num_tg_nodes);

    MatrixXf mat_ones(dlg.nodes.size(), dlg.nodes.size());
    for (const auto &n: dlg.nodes) {
        for (const auto &edge: n.outedges) {
            int a = edge->u;
            int b = edge->v;
            mat_ones(a, b) = 1;
        }
    }

    MatrixXf I(dlg.nodes.size(), dlg.nodes.size());
    I.setIdentity();

    {
        MatrixXf mat = mat_ones * params.beta;
        MatrixXf R = (I - mat);
        MatrixXf R_inv = R.inverse();

        VectorXf w(dlg.nodes.size());
        w.setOnes();
        w = R_inv * w;

        for (unsigned int j = 0; j < w.size(); ++j) {
            auto num_walks = w[j];
            auto &dlg_u = dlg.nodes[j];
            outwalks[dlg_u.e.u_id][dlg_u.e.t] += num_walks;
        }
    }
    {
        MatrixXf mat_trans = (mat_ones * params.alpha).transpose();
        MatrixXf R2 = (I - mat_trans);
        MatrixXf R2_inv = R2.inverse();

        VectorXf w(dlg.nodes.size());
        w.setOnes();
        w = R2_inv * w;
        for (unsigned int j = 0; j < w.size(); ++j) {
            auto num_walks = w[j];
            auto &dlg_v = dlg.nodes[j];
            inwalks[dlg_v.e.v_id][dlg_v.e.t + dlg_v.e.traversal_time] += num_walks;
        }
    }
    return {inwalks, outwalks};
}


Result
calculateWalkCentralityDLGMatrixNewVersionNonSparse(TemporalGraphStream &tgs, TemporalGraph &tg, Params const &params) {
    TopkResult topkResult(params.k, tgs.num_nodes);

    SGLog::log() << "Walk centrality (DLG Matrix non-sparse):" << endl;

    Timer timer;

    SGLog::log() << "computing DLG" << endl;

    timer.start();
    vector<double> centrality(tgs.num_nodes, 0);
    auto dlg = temporalGraphToDirectedLineGraph(tgs, tg);
    timer.stopAndPrintTime();

    SGLog::log() << "#nodes: " << dlg.nodes.size() << endl;
    SGLog::log() << "#edges: " << dlg.num_edges << endl;

    auto walks = computeVecMatricesInf1NonSparse(dlg, params);
    auto outwalks = walks.second;
    auto inwalks = walks.first;

    centrality = computeCentrality(tgs, inwalks, outwalks);

    auto final_time = timer.stopAndPrintTime();

    std::vector<std::pair<NodeId, double>> all_results;
    for (int i = 0; i < tgs.num_nodes; ++i) {
        all_results.emplace_back(i, centrality.at(i));
        topkResult.insert(centrality.at(i), i);
    }

    topkResult.print();

    Result result(final_time, {0, 1}, topkResult);
    result.all_results = all_results;
    return result;
}

