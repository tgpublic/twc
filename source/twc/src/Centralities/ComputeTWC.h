#ifndef TWC_COMPUTETWC_H
#define TWC_COMPUTETWC_H

#include <map>
#include "../Helpers/ResultFile.h"
#include "../Helpers/Params.h"
#include <list>

std::vector<double> computeCentrality(TemporalGraphStream const &tgs, std::vector<std::map<Time, double>> &ifct,
                                      std::vector<std::map<Time, double>> &ofct);

#endif //TWC_COMPUTETWC_H
