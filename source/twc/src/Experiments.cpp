#include "Experiments.h"
#include "Helpers/SGLog.h"
#include <fstream>
#include <Centralities/DLGMatrixTWC.h>
#include "TemporalGraph/DirectedLineGraph.h"
#include "Centralities/ComputeTWC.h"
#include "Helpers/HelperFunctions.h"
#include "Centralities/StreamTWCTime.h"
#include "Centralities/StreamTWC.h"

using namespace std;

void compute_centrality(Params &params, TemporalGraphStream tgs, TemporalGraph tg) {
    SGLog::log() << "Computing centrality" << endl;

    Result result;

    switch (params.centrality) {
        case TWC_Stream: {
            result = calculateWalkCentralityStreamNewVersion(tgs, params);
            break;
        }
        case TWC_Matrix: {
            result = calculateWalkCentralityDLGMatrixNewVersion(tgs, tg, params);
            break;
        }
        case TWC_Stream_Time: {
            result = calculateWalkCentralityStreamNewVersionTime(tgs, params);
            break;
        }
        case TWC_Matrix_NonSparse: {
            result = calculateWalkCentralityDLGMatrixNewVersionNonSparse(tgs, tg, params);
            break;
        }
        case TWC_Matrix_Approx: {
            result = calculateWalkCentralityDLGMatrixApproximationNewVersion(tgs, tg, params);
            break;
        }
        default:
            return;
    }

    if (!params.result_filename.empty()) {
        vector<pair<NodeId, double>> centrality;
        for (auto &p : result.all_results)
            centrality.emplace_back(p);
        double all_walks = 0;
        for (auto &c : result.all_results) {
            all_walks += c.second;
        }
        for (auto & p : centrality) {
            p.second /= all_walks;
        }
        HF::writeVectorToFile(params.result_filename, centrality, ",");
    }

}

