#ifndef CENTRALITY_TOPKRESULT_H
#define CENTRALITY_TOPKRESULT_H

#include "../TemporalGraph/TemporalGraphs.h"
#include <set>
#include <memory>
#include <sstream>

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6)
{
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return out.str();
}

struct comp
{
    template<typename T>
    bool operator()(const T& l, const T& r) const {
        return l.closeness > r.closeness;
    }
};

struct TopKEntry {

    TopKEntry(double closeness, NodeId nid) : closeness(closeness) {
        nids.push_back(nid);
    };

    ~TopKEntry() = default;

    double closeness = 0;
    std::vector<NodeId> nids;
};

class TopkResult {

public:

    TopkResult() = default;

    explicit TopkResult(const unsigned int k, const unsigned int num_nodes) :
        k(k), num_nodes(num_nodes), minMaxTopK(MAX_UINT_VALUE), h(num_nodes) {
    };

    explicit TopkResult(const unsigned int k, const unsigned int num_nodes, const unsigned int h) :
            k(k), num_nodes(num_nodes), minMaxTopK(MAX_UINT_VALUE), h(h) {};

    void insert(double closeness, NodeId nid);

    void print();

    [[nodiscard]] bool doApproximation() const {
        return topk.size() >= k;
    }

private:

    unsigned int k{};

    unsigned int num_nodes{}, h{};

    double minMaxTopK{};

    std::set<TopKEntry, comp> topk;

    std::vector<NodeId> allnids;

    std::string getResultString();
};


#endif //CENTRALITY_TOPKRESULT_H
