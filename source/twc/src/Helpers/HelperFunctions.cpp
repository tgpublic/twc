#include "HelperFunctions.h"

using namespace std;

std::ostream &operator<<(std::ostream &os, const TGStats &tgStats) {
    os << "\n# nodes: " << tgStats.num_nodes << std::endl;
    os << "# edges: " << tgStats.num_edges << std::endl;
    os << "# times: " << tgStats.num_times << std::endl;
    os << "tau_max: " << (tgStats.max_starting_times > tgStats.max_arrival_times ? tgStats.max_starting_times
                                                                                 : tgStats.max_arrival_times)
       << std::endl;
    os << "max starting times (tau^+_max): " << tgStats.max_starting_times << std::endl;
    os << "max arrival times (tau^-_max): " << tgStats.max_arrival_times << std::endl;
    os << std::endl;
    return os;
}

namespace HF {

    TGStats getTemporalGraphStreamsStatistics(TemporalGraphStream &tgs) {

        TGStats tgStats;

        tgStats.num_edges = tgs.edges.size();
        tgStats.num_nodes = tgs.num_nodes;

        if (tgStats.num_edges == 0 || tgStats.num_nodes == 0) {
            std::cout << "num_edges or num_nodes 0" << std::endl;
            return tgStats;
        }

        std::vector<std::unordered_set<Time>> starting_times(tgs.num_nodes, std::unordered_set<Time>());
        std::vector<std::unordered_set<Time>> arrival_times(tgs.num_nodes, std::unordered_set<Time>());

        std::unordered_set<Time> times;

        for (auto &e: tgs.edges) {
            times.insert(e.t);
            starting_times.at(e.u_id).insert(e.t);
            arrival_times.at(e.v_id).insert(e.t + e.traversal_time);
            if (starting_times.at(e.u_id).size() > tgStats.max_starting_times)
                tgStats.max_starting_times = starting_times.at(e.u_id).size();
            if (arrival_times.at(e.v_id).size() > tgStats.max_arrival_times)
                tgStats.max_arrival_times = arrival_times.at(e.v_id).size();

        }

        tgStats.num_times = times.size();
        return tgStats;
    }

}


