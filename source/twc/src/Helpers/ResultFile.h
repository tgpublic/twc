#ifndef CENTRALITY_RESULTFILE_H
#define CENTRALITY_RESULTFILE_H

#include <utility>
#include <string>
#include "../TemporalGraph/TemporalGraphs.h"
#include "../Experiments.h"
#include "../Helpers/TopkResult.h"


struct Result {
    Result(double running_time, std::pair<Time, Time> interval, TopkResult topkResult) :
            resultstr(std::to_string(running_time) + " s"), interval(std::move(interval)), topkResult(std::move(topkResult)) {}

    Result(std::string resultstr, std::pair<Time, Time> interval, TopkResult topkResult) :
            resultstr(std::move(resultstr)), interval(std::move(interval)), topkResult(std::move(topkResult)) {}

    explicit Result(std::string str) : resultstr(std::move(str)) { }

    Result() = default;

    std::string resultstr;
    std::pair<Time, Time> interval;
    TopkResult topkResult;

    std::vector<std::pair<NodeId, double>> all_results;
};

#endif //CENTRALITY_RESULTFILE_H
