#include "TopkResult.h"
#include "../Helpers/SGLog.h"
#include "../Helpers/HelperFunctions.h"

using namespace std;

void TopkResult::insert(double closeness, NodeId nid) {
    if (doApproximation() && minMaxTopK > closeness)
        return;

    auto p = topk.insert(TopKEntry(closeness, nid));
    if (!p.second) {
        const_cast<std::vector<NodeId>&>(p.first->nids).push_back(nid);
    }

    if (minMaxTopK < closeness && topk.size() > k) {
        auto i = topk.find(TopKEntry(minMaxTopK, 0));
        if (i != topk.end()) {
            topk.erase(i);
        }
    }

    minMaxTopK = topk.rbegin()->closeness;
}

void TopkResult::print() {
    SGLog::log() << getResultString();
}

string TopkResult::getResultString() {
    string result;
    unsigned int max_lines = 12;
    for (auto &e : topk) {
        result.append(to_string_with_precision(e.closeness, 20) + "\t" + to_string((double)e.closeness / h) + "\t");
        auto max_nids = 10;
        for (auto &n : e.nids) {
            result.append(to_string(n) + " ");
            allnids.push_back(n);
            max_nids--;
            if (max_nids == 0) {
                result.append("...");
                break;
            }
        }
        result.append("\n");
        max_lines--;
        if (max_lines == 0) {
            result.append("...\n");
            return result;
        }
    }
    return result;
}


