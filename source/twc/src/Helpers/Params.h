#ifndef TGPR_PARAMS_H
#define TGPR_PARAMS_H

#include <iostream>
#include "../TemporalGraph/TemporalGraphs.h"

enum MODES {
    comp_centrality,
    tg_statistics,
    approx_accuracy,
};


enum Centrality {
    TWC_Stream,
    TWC_Stream_Time,
    TWC_Matrix,
    TWC_Matrix_Approx,
    TWC_Matrix_NonSparse,
};


struct Params {
    std::string dataset_path;
    unsigned int mode = 0;
    unsigned int k = 0;
    unsigned int random_seed = 1;
    std::string result_filename;
    double alpha = 1.0;
    double beta = 1.0;
    double epsilon = 0.001;
    unsigned int delta = 1;
    Centrality centrality;
    bool directed = true;
    bool parseArgs(std::vector<std::string> args);
};

void show_help();

#endif //TGPR_PARAMS_H
