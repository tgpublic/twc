#include <valarray>
#include <Centralities/DLGMatrixTWC.h>
#include <Centralities/StreamTWC.h>
#include <cassert>
#include "AccuracyTest.h"
#include "SGLog.h"
#include "Params.h"
#include "HelperFunctions.h"

using namespace std;


double mape(vector<std::pair<NodeId, double>> const &exact, vector<std::pair<NodeId, double>> const &approx) {
    double n = 0;
    double mape = 0;
    for (int i = 0; i < exact.size(); ++i) {
        if (exact[i].second > 0) {
            double abs_dif = abs(exact[i].second - approx[i].second) / exact[i].second;
            mape += abs_dif;
            n += 1.0;
        } else {
            assert(exact[i].second == approx[i].second);
        }
    }
    if (n == 0) return 0;
    return mape / n;
}


void test_accuracy(TemporalGraphStream tgs, TemporalGraph tg, const Params &params) {

    auto result_approx = calculateWalkCentralityDLGMatrixApproximationNewVersion(tgs, tg, params);
    auto result_exact = calculateWalkCentralityStreamNewVersion(tgs, params);

    auto app = result_approx.all_results;
    auto exa = result_exact.all_results;

    auto mae_val = mape(exa, app);
    SGLog::log() << "Avg. Relative Error: " << mae_val << endl;

}