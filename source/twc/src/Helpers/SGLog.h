#ifndef CENTRALITY_SGLOG_H
#define CENTRALITY_SGLOG_H

#include <string>
#include <fstream>
#include <iostream>
#include <chrono>

constexpr char logfilename[] = "logfile.txt";

class SGLog {

public:

    SGLog(const SGLog&) = delete;

    SGLog& operator= (const SGLog&) = delete;

    static SGLog& log() {
        static SGLog instance;
        return instance;
    }

    ~SGLog() {
        logstream << "\nStopped logging at" << std::endl << getTime() << std::endl;
        logstream.close();
    }

    SGLog& operator<< (std::ostream& (*f)(std::ostream &)) {
        f(logstream);
        f(std::cout);
        return *this;
    }

    SGLog& operator<< (std::ostream& (*f)(std::ios &)) {
        f(logstream);
        f(std::cout);
        return *this;
    }

    SGLog& operator<< (std::ostream& (*f)(std::ios_base &)) {
        f(logstream);
        f(std::cout);
        return *this;
    }

    template<typename T>
    SGLog& operator<<(const T &msg) {
        logstream << msg;
        std::cout << msg << std::flush;
        return *this;
    }

private:

    SGLog() {
        logstream.open(logfilename /*+ getTime()*/, std::fstream::out | std::fstream::app);
        logstream << "\n----------------------------------------------------------\nStart logging at" << std::endl << getTime() << std::endl;
    }

    static std::string getTime() {
        std::chrono::system_clock::time_point p =  std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(p);
        return std::ctime(&t);
    }

    std::ofstream logstream;

};


class Timer {
public:
    void start() {
        timePoint = std::chrono::high_resolution_clock::now();
    }

    double stop() {
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - timePoint;
        return elapsed.count();
    }

    double stopAndPrintTime() {
        auto t = stop();
        SGLog::log() << "Elapsed: " << t << std::endl;
        return t;
    }


private:
    std::chrono::high_resolution_clock::time_point timePoint{};
};

#endif //CENTRALITY_SGLOG_H
