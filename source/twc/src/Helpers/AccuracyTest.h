#ifndef TGPR_ACCURACYTEST_H
#define TGPR_ACCURACYTEST_H

#include "../Helpers/Params.h"

void test_accuracy(TemporalGraphStream tgs, TemporalGraph tg, const Params& params);


#endif //TGPR_ACCURACYTEST_H
