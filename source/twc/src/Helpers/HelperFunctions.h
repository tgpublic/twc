#ifndef CENTRALITY_HELPERFUNCTIONS_H
#define CENTRALITY_HELPERFUNCTIONS_H

#include <list>
#include <set>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "TemporalGraph/TemporalGraphs.h"

struct TGStats {
    unsigned long num_nodes = 0;
    unsigned long num_edges = 0;
    unsigned long num_times = 0;
    unsigned long max_starting_times = 0;
    unsigned long max_arrival_times = 0;
};

std::ostream& operator<<(std::ostream& os, const TGStats& ts);

namespace HF {

    TGStats getTemporalGraphStreamsStatistics(TemporalGraphStream &tgs);

    template<typename T, typename R>
    void writeVectorToFile(const std::string& filename, std::vector<std::pair<T,R>> data, const std::string& sep) {
        std::ofstream fs;
        fs.open(filename);
        if (!fs.is_open()) {
            std::cout << "Could not write data to " << filename << std::endl;
            return;
        }
        fs << std::setprecision(50);
        for (auto &d : data)
            fs << d.first << sep << d.second << std::endl;

        fs.close();
    }

}
#endif //CENTRALITY_HELPERFUNCTIONS_H
