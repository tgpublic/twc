#include "DirectedLineGraph.h"

using namespace std;

DirectedLineGraph reverseDLG(DirectedLineGraph const &dlg) {
    DirectedLineGraph rdlg = dlg;
    for (auto &n : rdlg.nodes) {
        n.inedges.clear();
        n.outedges.clear();
    }
    for (unsigned long nid = 0; nid < rdlg.nodes.size(); nid++) {
        for (auto &e : dlg.nodes.at(nid).outedges) {
            auto rev = make_shared<DLEdge>(e->getInverseEdge());
            rdlg.nodes.at(rev->u).outedges.push_back(rev);
            rdlg.nodes.at(rev->v).inedges.push_back(rev);
        }
    }
    rdlg.reversed = true;
    return rdlg;
}

DirectedLineGraph temporalGraphToDirectedLineGraph(TemporalGraphStream const &tgs, TemporalGraph const &tg) {
    DirectedLineGraph dlg;

    dlg.nodes.clear();
    dlg.nodes.resize(tgs.edges.size());
    vector<bool> nodeset(tgs.edges.size(), false);

    for (auto &tgn : tg.nodes) {
        for (auto &e : tgn.adjlist) {
            if (!nodeset.at(e.id)) {
                DLNode node;
                node.id = e.id;
                node.e = e;
                dlg.nodes.at(node.id) = node;
                nodeset.at(e.id) = true;
            }
            for (auto &f : tg.nodes[e.v_id].adjlist) {
                if (e.t + e.traversal_time > f.t) continue;
                if (!nodeset.at(f.id)) {
                    DLNode node;
                    node.id = f.id;
                    node.e = f;
                    dlg.nodes.at(node.id) = node;
                    nodeset.at(f.id) = true;
                }
                auto &m = dlg.nodes.at(f.id);
                auto &n = dlg.nodes.at(e.id);
                auto de = make_shared<DLEdge>();
                de->u = n.id;
                de->v = m.id;
                de->u_t = n.e.t;
                de->v_t = m.e.t;
                de->tg_u = n.e.u_id;
                de->tg_w = m.e.v_id;
                de->tg_v = n.e.v_id;
                n.outedges.push_back(de);
                m.inedges.push_back(de);
                dlg.num_edges++;
            }
        }
    }
    dlg.num_tg_nodes = tgs.num_nodes;

    return dlg;
}
