#ifndef CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
#define CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H

#include <cassert>
#include "TemporalGraphs.h"

class TemporalGraphStreamProvider {

public:

    void loadTemporalGraph(const std::string &path, bool directed);

    TemporalGraphStream &getTGS() { return tgs; }

    TemporalGraph &getTG() {
        return tg;
    }

private:

    TemporalGraphStream tgs;

    TemporalGraph tg;

};


#endif //CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
