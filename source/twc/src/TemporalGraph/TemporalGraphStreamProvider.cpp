#include "TemporalGraphStreamProvider.h"
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <set>

using namespace std;


void getFileStream(const string &filename, ifstream &fs) {
    fs.open(filename);
    if (!fs.is_open()) {
        cout << "Could not open data set " << filename << endl;
        exit(EXIT_FAILURE);
    }
}

vector<unsigned long> split_string(const string &s) {
    vector<unsigned long> result;
    stringstream ss(s);
    while (ss.good()) {
        string substr;
        getline(ss, substr, ' ');
        result.push_back(stoul(substr));
    }
    return result;
}

void TemporalGraphStreamProvider::loadTemporalGraph(const std::string &path, bool directed) {

    ifstream fs;
    getFileStream(path, fs);
    string line;
    getline(fs, line);
    unsigned long num_nodes = stoul(line);
    TemporalEdges tes;
    EdgeId eid = 0;

    Time mintime = MAX_UINT_VALUE;

    unordered_set<string> eds;

    unsigned long max_lines = MAX_UINT_VALUE;
    while (getline(fs, line) && max_lines-- > 0) {
        vector<unsigned long> l = split_string(line);
        NodeId u = l[0];
        NodeId v = l[1];
        Time t = l[2];
        Time tt = 1;

        string s1 = to_string(u) + "-" + to_string(v) + "-" + to_string(t) + "-" + to_string(tt);
        if (eds.find(s1) == eds.end() && u != v) {
            TemporalEdge e1 = TemporalEdge(u, v, t + 1, 0, tt, eid++);
            tes.push_back(e1);
            eds.insert(s1);
            if (e1.t < mintime) mintime = e1.t;
        }
        if (!directed) {
            string s2 = to_string(v) + "-" + to_string(u) + "-" + to_string(t) + "-" + to_string(tt);
            if (eds.find(s2) == eds.end()) {
                TemporalEdge e2 = TemporalEdge(v, u, t + 1, 0, tt, eid++);
                tes.push_back(e2);
                eds.insert(s2);
                if (e2.t < mintime) mintime = e2.t;
            }
        }
    }
    fs.close();

    for (auto &e: tes) {
        e.t -= mintime;
    }
    tgs.edges = tes;
    tgs.sort_edges();
    tgs.num_nodes = num_nodes;
    tg = tgs.toTemporalGraph();
}

